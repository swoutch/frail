"""
    Contains functions to search available trains on oui.sncf
"""

from frail.core import search
from frail.passenger import Passenger

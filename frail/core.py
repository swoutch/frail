"""
    How to use it:
    >>> import frail, pendulum
    >>> trains = frail.search("FRPAR", "FRMRS", timestamp=pendulum.now())
"""
import json
import string

import pendulum
import requests

import frail.train

__all__ = ["search"]


def search(origin, destination, timestamp, passenger=None):
    """
        Returns list of trains objects around specified timestamp.
    """

    # build request body
    with open("frail/body-template.json") as file:
        request_body = file.read()
    request_body = string.Template(request_body).substitute(
        destination=destination,
        origin=origin,
        timestamp=timestamp.format("YYYY-MM-DDTHH:mm:ss"),
    )
    if passenger:
        dict_body = json.loads(request_body)
        dict_body["wish"]["passengers"][0]["discountCard"] = {
            "code": "HAPPY_CARD",
            "dateOfBirth": passenger.birth_date.format("YYYY-MM-DD"),
            "number": passenger.id,
        }
        request_body = json.dumps(dict_body)
    # requests the API
    response = requests.post(
        url="https://www.oui.sncf/proposition/rest/travels/outward/train",
        data=request_body,
        headers={"Content-Type": "application/json"},
    )

    # extract data from response
    trains = []
    for travel in response.json()["travelProposals"]:
        train = frail.train.Train(
            origin=travel["origin"]["station"]["metaData"]["MI"]["code"],
            destination=travel["destination"]["station"]["metaData"]["MI"]["code"],
            departure=pendulum.parse(travel["departureDate"], tz="Europe/Paris"),
            arrival=pendulum.parse(travel["arrivalDate"], tz="Europe/Paris"),
            price=travel["minPrice"],
        )
        trains.append(train)
    return trains

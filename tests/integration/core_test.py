import os
import pendulum

import frail


def test_search():
    trains = frail.search(
        "FRPAR",
        "FRMRS",
        # timestamp=pendulum.now(tz="Europe/Paris"),
        timestamp=pendulum.datetime(2019, 10, 23, 12),
    )
    assert len(trains) > 0
    assert trains[0].price >= 0


def test_search_as_tgvmax_user():
    passenger = frail.Passenger(
        id=os.getenv("TGVMAX_ID"),
        birth_date=pendulum.from_format(os.getenv("TGVMAX_BIRTHDATE"), "DD/MM/YYYY"),
    )
    trains = frail.search(
        "FRPAR", "FRMRS", timestamp=pendulum.now(tz="Europe/Paris"), passenger=passenger
    )
    assert len(trains) > 0
